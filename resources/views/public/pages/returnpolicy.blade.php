@extends('public.layouts.master')

@section('seo')
<meta name="description" content="{{$returnpolicy->description}}">
<meta name="keywords" content="{{$returnpolicy->tag}}">
@endsection

@section('title')
<title>Return Policy</title>
@endsection

@section('custom-css')
<style>
	#about-section{
		margin-top: -70px;
	}
    .page-title{
        background-image: url('{{ set_path("assets/img/pages/breadcrumb_bg01.jpg") }}');
    }
</style>
@endsection


@section('content')


<section class="page-title">
    <div class="breadcrumb-content">
        <h2>Return Policy</h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('website.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Return Policy</li>
            </ol>
        </nav>
    </div>
</section>

<section class="page-content">
    <div class="container">
        {!! $returnpolicy->returnpolicy !!}
    </div>
</section>



	
@endsection


@section('custom-js')
@endsection