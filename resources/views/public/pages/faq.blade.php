@extends('public.layouts.master')

@section('seo')
<meta name="description" content="{{$settings->description}}">
<meta name="keywords" content="{{$settings->tag}}">
@endsection

@section('title')
<title>FAQ | {{$settings->title}}</title>
@endsection
@section('custom-css')
<style>
	.page-title{
		background-image: url('{{ set_path("assets/img/pages/breadcrumb_bg01.jpg") }}');
	}
	.faq_title{
		cursor: pointer;
	}
</style>
@endsection


@section('content')

<section class="page-title">
		<div class="breadcrumb-content">
			<h2>FAQ</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ route('website.home') }}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">FAQ</li>
				</ol>
			</nav>
		</div>
	</section>

	<section id="faq-section">
		<div class="container py-5">
			<div class="row">
				<div class="col-12">
					<div class=" text-justify">
						<div id="accordion">

 						@foreach($faqs as $faq)

						  <div class="card mb-1">
						    <div class="card-header bg-success" id="heading-{{$faq->id}}">
						      <h5 class="mb-0 text-light faq_title" data-toggle="collapse" data-target="#collapse-{{$faq->id}}" aria-expanded="true" aria-controls="collapse-{{$faq->id}}">
						          {{ $faq->question }}
						      </h5>
						    </div>

						    <div id="collapse-{{$faq->id}}" class="collapse" aria-labelledby="heading-{{$faq->id}}" data-parent="#accordion">
						      <div class="card-body">
						        {{$faq->ans}}
						      </div>
						    </div>
						  </div>
						@endforeach

	

					</div>
				</div>
			</div>
		</div>
	</section>
@endsection


@section('custom-js')
@endsection