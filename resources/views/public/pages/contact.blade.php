@extends('public.layouts.master')

@section('seo')
<meta name="description" content="{{$settings->description}}">
<meta name="keywords" content="{{$settings->description}}">
@endsection

@section('title')
<title>Contact Us</title>
@endsection

@section('custom-css')
	<style>
		input,textarea{
			border-radius: 0px !important;
		}
		#send_btn{
			padding: 5px 50px;
			border: 0px;
			background: #49a010;
			color: #fff;
			cursor: pointer;
		}
		.read-border{
			border: 1px solid red !important;
			outline: none !important;
			box-shadow: 0px 0px 0px !important;
		}
		.page-title{
			background-image: url('{{ set_path("assets/img/pages/breadcrumb_bg01.jpg") }}');
		}
	</style>
@endsection



@section('content')
	<section class="page-title">
		<div class="breadcrumb-content">
			<h2>Contact Us</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ route('website.home') }}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Contact Us</li>
				</ol>
			</nav>
		</div>
	</section>

	<section id="contact-section">
		<div class="container py-5">
			<div class="row">
				<div class="col-12">
					<div class=" text-justify">					
						
						<div class="row my-4">
							<div class="col-12 col-lg-7">
								@if(isset($result))
								<div class="alert alert-success">Email Send Successfull</div>
								@else
								<form action="{{ route('website.email.send') }}" method="post">
								@csrf
								<h2 style="font-size: 20px;" class="text-center">Email Us</h2>
								<hr class="my-2">
								<div class="row">
									<div class="col-12">
										<label for="name"><b>Name:</b></label>
										<input maxlength="200" value="{{ old('name') }}" type="text" class="form-control mb-2 " name="name" placeholder="Your Full Name">
										@error('name')
											<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>

								<div class="row mb-2">
									<div class="col-12">
										<label for="subject"><b>Subject<span class="text-danger">*</span></b></label>
										<input maxlength="150" value="{{ old('subject') }}" type="text" class="form-control" name="subject" placeholder="Subject">
										@error('subject')
											<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>

								<div class="row mb-2">
									<div class="col-12 col-lg-6">
										<label for="email"><b>Email:<span class="text-danger">*</span></b></label>
										<input maxlength="100" type="text" value="{{ old('email') }}" class="form-control" name="email" placeholder="Your Email">
										@error('email')
											<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
									<div class="col-12 col-lg-6">
										<label for="phone"><b>Phone:<span class="text-danger">*</span></b></label>
										<input maxlength="15" type="text" value="{{ old('phone') }}" class="form-control" name="phone" placeholder="Your Phone">
										@error('phone')
											<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>

								<div class="row mb-2">
									<div class="col-12">
										<label for="message"><b>Message<span class="text-danger">*</span></b></label>
										<textarea  name="message" cols="30" rows="5"class="form-control" placeholder="Your Message">{{ old('message') }}</textarea>
										@error('message')
											<span class="text-danger">{{ $message }}</span>
										@enderror
									</div>
								</div>

								<div class="text-right">
									<input type="submit" id="send_btn" class="btn_1 full-width mt-3 form-control" value="Send">
								</div>
								</form>
								@endif
							</div>

							<div class="col-12 col-lg-5">
								<h2 style="font-size: 20px;" class="text-center">Contact Information</h2>
								<hr class="my-2">
								<p>
									Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure natus saepe, aperiam, necessitatibus nulla harum ratione facilis tenetur provident. Quaerat dignissimos, modi recusandae nesciunt velit quasi fugiat. Iure, provident officia.
									<br>

									Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Alias doloremque rerum esse adipisci! Eius natus modi repellendus similique quidem reiciendis deleniti at, culpa impedit, nam nihil deserunt, odit nostrum, hic.
								</p>
								<br>
								<address>
									<p class="mb-2"><b>Address: </b> 	{{$settings->address}}</p>
									<p class="mb-2"><b>Phone: </b> 		{{$settings->phone}}</p>
									<p class="mb-2"><b>Email: </b> 		{{$settings->email}}</p>
									
								</address>
								<br>
								<h2 class="ft__title">Follow Us</h2>
								<ul class="social__icon">
								    <li><a href="https://twitter.com/" target="_blank"><i class="zmdi zmdi-twitter"></i></a></li>
								    <li><a href="https://www.instagram.com/" target="_blank"><i class="zmdi zmdi-instagram"></i></a></li>
								    <li><a href="https://www.facebook.com/" target="_blank"><i class="zmdi zmdi-facebook"></i></a></li>
								    <li><a href="https://plus.google.com/" target="_blank"><i class="zmdi zmdi-google-plus"></i></a></li>
								</ul>
							</div>
						</div>



						<div>
							<iframe src="{{Session::get('location')}}" width="100%" height="550" frameborder="0" style="border:2px solid #49a010; border-radius: 20px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection


@section('custom-js')



@endsection