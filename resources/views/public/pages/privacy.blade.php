@extends('public.layouts.master')

@section('seo')
<meta name="description" content="{{$privacy->description}}">
<meta name="keywords" content="{{$privacy->tag}}">
@endsection

@section('title')
<title>Privacy Policy</title>
@endsection

@section('custom-css')
<style>
	.page-title{
		background-image: url('{{ set_path("assets/img/pages/breadcrumb_bg01.jpg") }}');
	}
</style>
@endsection

@section('content')
	<section class="page-title">
		<div class="breadcrumb-content">
			<h2>Privacy Policy</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ route('website.home') }}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
				</ol>
			</nav>
		</div>
	</section>
	<section class="page-content py-5">
		<div class="container">
		{!! $privacy->privacy !!}
		</div>
	</section>
@endsection


@section('custom-js')
@endsection