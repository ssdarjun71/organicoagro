@extends('public.layouts.master')

@section('seo')
<meta name="description" content="{{$settings->description}}">
<meta name="keywords" content="{{$settings->tag}}">
@endsection


@section('title')
<title>Login | {{$settings->title}}</title>
@endsection

@section('custom-css')


@endsection


@section('content')

<div class="container py-5">
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <div class="card login-card">
                <div class="card-header text-center">
                    <h1>Login</h1>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input  id="email" type="email" class="form-control rounded-0 @error('email') is-invalid @enderror" name="email" value="customer1@gmail.com" readonly required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                            <input value="22222222" readonly id="password" type="password" class="form-control rounded-0 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-check mb-2">
                            <input class="form-check-input " type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>

                        <div class="form-group mb-0">
                            <button type="submit" class="btn btn-primary btn-sm submit-btn">{{ __('Login') }}</button>

                            @if (Route::has('password.request'))
                                <a class="float-right" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </form>
                </div>
                <div class="card-footer text-center">
                    New member? <a class="text-info" href="{{route('website.customer.registration')}}">Register</a> here.
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('custom-js')
@endsection