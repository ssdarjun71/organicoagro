<div class="card p-2 product-card">
    <div class="product-img mb-3 text-center">
        <img src="{{set_path('assets/img/products/')}}/{{$product->image}}" alt="product images">
    </div>

    <a href="{{route('website.single_product',['slug' => $product->slug])}}">
        <div class="product-name">
            <h3>{{$product->name}}</h3>
        </div>
    </a>
    
    <div class="product-price">
        @if($product->price != $product->old_price)
            <h6><del>৳ {{$product->old_price}}</del>৳ {{$product->price}}</h6>
        @else
            <h6>৳ {{$product->price}}</h6>
        @endif
        
        <div class="product-rating">

            <i class="add-wishlist fas fa-heart mr-2 text-secondary <?php if(in_array($product->id, $wishlist_products)){ echo 'active'; }?>" product-id="{{$product->id}}"></i>
                
            <i class="fas fa-star"></i><span>4.5/2</span>
        </div>
    </div>
    <div class="product-btn">
        <a class="add-to-cart-single-btn" href="#"
        data-id="{{$product->id}}" 
        data-price="{{$product->price}}"
        data-unit="{{$product->unit}}"
        data-name="{{$product->name}}"
        data-image="{{$product->image}}"
        data-code="{{$product->code}}"
        data-slug="{{$product->slug}}"
        ><i class="fas fa-shopping-basket"></i><span>Add to Cart</span></a>
    </div>
</div>