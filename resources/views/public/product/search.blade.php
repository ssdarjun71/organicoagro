@extends('public.layouts.master')

@section('seo')
<meta name="description" content="{{$settings->description}}">
<meta name="keywords" content="{{$settings->tag}}">
@endsection

@section('title')
<title>{{$seo_data['title']}}</title>
@endsection


@section('custom-css')
	<style>
		#about-section{
			margin-top: -70px;
		}
		.page-title{
			background-image: url('{{ set_path("assets/img/pages/breadcrumb_bg01.jpg") }}');
		}
	</style>


@endsection


{{-- main content --}}
@section('content')

<section class="page-title">
    <div class="breadcrumb-content">
        <h2>{{$seo_data['title']}}</h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('website.home') }}">Search Result</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$seo_data['title']}}</li>
            </ol>
        </nav>
    </div>
</section>


	<!-- Start Our ShopSide Area -->
	<section class="htc__shop__sidebar  ">
	    <div class="container">
			<div class="row my-3">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<!-- Start Range -->
					<div class="htc-grid-range border-0 mb-0 pb-0">
						<div class="content-shopby">
							<div class="price_filter s-filter clear">
								<form action="{{route('website.shop_filter')}}" method="POST">
									@csrf
									<div id="slider-range"></div>
									<div class="slider__range--output">
										<div class="price__output--wrap">
											<div class="price--output">
												<span>Price :</span><input name="price" type="text" id="amount" readonly>
											</div>
											<div class="price--filter">
												{{-- <a type="submit" href="#">Filter</a> --}}
												<input class="border-0 bg-dark text-light px-3 py-0" type="submit" value="Filter">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- End Range -->
				</div>
			</div>
	        <div class="row mb-5">
	            <div class="col-md-12 col-lg-12 order-lg-12 order-1 col-sm-12 col-xs-12 smt-30">
	                <div class="tab-contet shop__grid__view__wrap">
	                    <!-- Start Single View -->
	                    <div role="tabpanel" id="grid-view" class="row single-grid-view tab-pane  active clearfix">
	                        @foreach($products as $product)
								<div class="col-6 col-md-3 mb-4 single__pro ">
									@include('public.product.product-card')
								</div>
	                        @endforeach
	                    </div>
	                    <!-- End Single View -->
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<!-- End Our ShopSide Area -->



@endsection
{{-- end main content --}}

