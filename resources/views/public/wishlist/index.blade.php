@extends('public.layouts.master')


@section('title')
<title>Wishlist</title>
@endsection

@section('custom-css')
<style>
	#about-section{
		margin-top: -70px;
	}
    .page-title{
        background-image: url('{{ set_path("assets/img/pages/breadcrumb_bg01.jpg") }}');
    }
</style>
@endsection


@section('content')


<section class="page-title">
    <div class="breadcrumb-content">
        <h2>Wishlist</h2>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('website.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Wishlist</li>
            </ol>
        </nav>
    </div>
</section>

<section class="page-content">
    <div class="container">
        <div class="row my-2">
            @foreach($products as $product)
                @if($product->home_show == 1)
                    <div class="col-6 col-md-3 single__pro ">
                        @include('public.product.product-card')
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</section>



	
@endsection


@section('custom-js')


@endsection