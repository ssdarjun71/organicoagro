@extends('public.layouts.master')

@section('seo')
<meta name="description" content="{{$seo_data['description']}}">
<meta name="keywords" content="{{$seo_data['tag']}}">
@endsection

@section('title')
<title>{{$seo_data['title']}}</title>
@endsection


@section('custom-css')
	
	<style>
		.single_product{
			position: relative;
		}
		.single_product .offer{
			position: absolute;
			z-index: 98;
			top: 0;
			right: 0;
			font-size: 12px;
			color:#fff;
			padding: 0px 3px;
		}
		
		.page-title{
			background-image: url('{{ set_path("assets/img/pages/breadcrumb_bg01.jpg") }}');
		}
	</style>

@endsection


{{-- main content --}}
@section('content')


	<!-- Start Our ShopSide Area -->
	<section class="htc__shop__sidebar  ">

	<section class="page-title">
		<div class="breadcrumb-content">
			<h2>{{$seo_data['title']}}</h2>
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ route('website.home') }}">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{$seo_data['title']}}</li>
				</ol>
			</nav>
		</div>
	</section>
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 col-lg-12 order-lg-12 order-1 col-sm-12 col-xs-12 smt-30">
	                <div class="tab-contet shop__grid__view__wrap mt-2">
	                    <!-- Start Single View -->
	                    <div role="tabpanel" id="grid-view" class="row single-grid-view tab-pane  active clearfix">

	                        @foreach($products as $product)
								<div class="col-6 col-md-3 single__pro ">
									@include('public.product.product-card')
								</div>
	                        	<!-- End Single Product -->
	                        @endforeach
	                    </div>
	                    <!-- End Single View -->
	                    
	                </div>

	                 <div id="pagination">
	                       	{{ $products->links() }}
	                 </div>
	            </div>
	        </div>
	    </div>
	</section>
	<!-- End Our ShopSide Area -->



@endsection
{{-- end main content --}}

