<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        {{-- 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        --}}
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="author" content="John Doe">
        @yield('seo')
        @yield('title')
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">

        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" type="image/x-icon" href="{{set_path('assets/img/favicon.ico')}}">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Bootstrap Fremwork Main Css -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/bootstrap.min.css')}}">
        <!-- All Plugins css -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/plugins.css')}}">
        <!-- Theme shortcodes/elements style -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/shortcode/shortcodes.css')}}">
        <!-- Theme main style -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/style.css')}}">
        <!-- Responsive css -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/responsive.css')}}">
        <!-- User style -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/custom.css')}}">
        <!-- Modernizr JS -->
        <script src="{{set_path('assets/front-end/js/vendor/modernizr-2.8.3.min.js')}}"></script>
        @yield('custom-css')
    </head>
    <body id="main-body">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->  
        <div id="notificaton-div">
            <span id="notificaton-text"></span>
        </div>
        <div id="cart-side" class="cart-side cart__menu">
            <span class="ti-shopping-cart"><span class="total_cart_products" ></span></span>
        </div>
        <!-- Body main wrapper start -->


            <!-- Start Header Style -->
            @include('includes.header')
            <!-- End Header Style -->
        </div>

        <div class="wrapper home-9 wrap__box__style--1">
            
            <div class="body__overlay"></div>

            <!-- Start Offset Wrapper -->
            <div class="offset__wrapper">

                <!-- Start Cart Panel -->
                <div class="shopping__cart">
                    <div class="shopping__cart__inner">
                        <div class="offsetmenu__close__btn">
                            <a href="#"><i class="zmdi zmdi-close"></i></a>
                        </div>
                        <div class="shp__cart__wrap" id="cart_items_list">
                            {{-- show cart product --}}
                        </div>
                        <ul class="shoping__total">
                            <li class="subtotal">Subtotal:</li>
                            <li class="total__price" id="total_price_of_cart">$00.00</li>
                        </ul>
                        <ul class="shopping__btn">
                            <li><a href="{{route('website.cart.view')}}">View Cart</a></li>
                            <li class="shp__checkout"><a href="{{route('website.cart.check_out')}}">Checkout</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Cart Panel -->
            </div>
            <!-- End Offset Wrapper -->

            <!-- main body -->
            <div class="page-content">
                <div class="bg--4 ">
                    @if(Request::is('/'))
                    <div class="container slider--four slider__new">
                        <div class="row">
                            <div class="col-lg-3 px-1 px-sm-2">

                            </div>
                            <div class="col-lg-7 pt-0 px-0 pt-sm-2 pr-sm-2">        
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <?php
                                            $slider_number = 0;
                                        ?>
                                        @foreach($sliders as $slider)
                                            <?php ++$slider_number; ?>
                                            @if($slider->active == 1)
                                            <div class="carousel-item {{ $slider_number }} <?php echo ($slider_number == 1 ? 'active' : ''); ?>">
                                                <img class="d-block w-100" src="{{set_path('assets/img/slider/')}}/{{$slider->image}}" alt="First slide">
                                            </div>
                                            @endif
                                        @endforeach                     
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div class="category-slider-area pt--10">
                                    <div class="owl-carousel owl-theme popular__product__wrap">
                                        @foreach($categories as $category)
                                        @if($category->id != 1)
                                        <div class="single-category item">
                                            <a href="{{route('website.single_category',['slug' => $category->slug])}}">
                                            {{-- <img src="https://via.placeholder.com/120x60" alt="{{$category->name}}"> --}}
                                            <img src="{{set_path('assets/img/category/')}}/{{$category->image}}" alt="{{$category->name}}">
                                            </a>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="text-right mr-5 mt-2 d-none d-lg-block">
                                        <a href="{{route('website.all_categories')}}" class="mr-4"><b>All Categories</b></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 pt-0 px-0 pt-sm-2 px-sm-2">
                                <div class="advertisment">
                                    <div class="row mx-0">
                                        <div class="col-6 col-lg-12 px-1 px-sm-2">
                                            <img class="pt-2 pt-sm-0" src="{{ set_path('assets/img/ads/FgeerGGO.Fast-&-secure-delevery.gif') }}" alt="">
                                        </div>
                                        <div class="col-6 col-lg-12 px-1 px-sm-2">
                                            <img class="pt-2" src="{{ set_path('assets/img/ads/FgeerGGO.Fast-&-secure-delevery.gif') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Slider Area -->
                    @endif
                    <!-- page content -->
                    @yield('content')
                    <!-- end page content -->

                    <!-- Start Footer Area -->
                        @include('includes.footer')
                    <!-- End Footer Area -->
                </div>
            </div>
            <!-- end main body -->
        </div>
        <!-- Body main wrapper end -->
        <!-- Placed js at the end of the document so the pages load faster -->
        <!-- jquery latest version -->
        <script src="{{set_path('assets/front-end/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <!-- Bootstrap Framework js -->
        <script src="{{set_path('assets/front-end/js/popper.min.js')}}"></script>
        <script src="{{set_path('assets/front-end/js/bootstrap.min.js')}}"></script>
        <!-- All js plugins included in this file. -->
        <script src="{{set_path('assets/front-end/js/plugins.js')}}"></script>

        <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
        
        <!-- Main js file that contents all jQuery plugins activation. -->
        <script src="{{set_path('assets/front-end/js/main.js')}}"></script>
        <script>
            // ajax call setup header 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // end header setup 
        </script>
        {{-- add to cart --}}
        <script src="{{set_path('assets/front-end/js/add-to-cart.js')}}"></script>

        {{-- wishlist --}}
        <script src="{{set_path('assets/front-end/js/wishlist.js')}}"></script>
        @yield('custom-js')
    </body>
</html>