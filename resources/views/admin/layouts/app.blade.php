<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OrganicoAgro') }}</title>

    <!-- Scripts -->
    <script src="{{ set_path('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ set_path('css/app.css') }}" rel="stylesheet">

    <style>
        body{
          background-color: #49a010;
        }
        .card{
            position: absolute;
            top: 30%;
            left: 50%;
            transform: translate(-50%,-50%);
            width: 600px;
        }
        .submit-btn{
           font-size: 18px;
           background: #49a010;
        }

        @media(max-width: 767px) {
            .card{
                top: 10%;
                transform: translate(-50%,0%);
                width: 95%;
            }
        }
    </style>
</head>
<body>
    <div id="app" >
        <main>
            @yield('content')
        </main>
    </div>
</body>
</html>
