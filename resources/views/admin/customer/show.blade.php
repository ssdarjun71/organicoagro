@extends('admin.layouts.master')

@section('title')
    <title>{{ $customer->name }}</title>
@endsection

@section('content')
    <!-- page title area  -->
    <div class="row">
        <div class="col-12 col-lg-6">
            <h1 class="font-pt font-30 pb-3">Customer Details</h1>
        </div>
        <div class="col-12 col-lg-6 text-right pb-3">
            <a data-toggle="tooltip" data-placement="top" title="Back" href="{{ url()->previous() }}"
                class="btn btn-primary"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-lg-12 mb-2">
            <div class="card p-3">
                <h1 class="text-center font-20 font-pt border-bottom pb-3">Personal Information</h1>
                <dl class="row mt-3">
                    <dt class="col-sm-3">Name</dt>
                    <div class="col-sm-1"><b>:</b></div>
                    <dd class="col-sm-8">{{ $customer->name }}</dd>

                    <dt class="col-sm-3">Email</dt>
                    <div class="col-sm-1"><b>:</b></div>
                    <dd class="col-sm-8">{{ $customer->email }}</dd>

                    <dt class="col-sm-3">Phone</dt>
                    <div class="col-sm-1"><b>:</b></div>
                    <dd class="col-sm-8">{{ $customer->phone }}</dd>

                    <dt class="col-sm-3">Address</dt>
                    <div class="col-sm-1"><b>:</b></div>
                    <dd class="col-sm-8">{{ $customer->address }}</dd>
                </dl>
            </div>
        </div>

        <div class="col-12 col-lg-12 mb-2">
            <div class="card p-3">
                <h1 class="text-center font-20 font-pt">All Reviews</h1>
                <hr>
                @php
                    $total_review = 0;
                    $total_active_review = 0;
                    $total_pending_review = 0;
                    $i = 0;
                @endphp
                <table class="table table-striped table-dark display custom-data-table" id="">
                    <thead>
                        <tr align="center">
                            <th scope="col">No</th>
                            <th scope="col">Comment</th>
                            <th scope="col">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customer->reviews as $review)
                            @php
                                $total_review++;
                                
                                if ($review->active == 1) {
                                    $total_active_review++;
                                } else {
                                    $total_pending_review++;
                                }
                            @endphp
                            <tr align="center">
                                <th scope="row">{{ $total_review }}</th>

                                <td><a target="_blank" class="text-info" href="{{ route('admin.review.show', ['id' => $review->id]) }}">{{ $review->comment }}</a>
                                </td>
                                <td>{{ $review->created_at->format('d-m-Y') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <input type="hidden" id="total_review" value="{{ $total_review }}">
                <input type="hidden" id="total_active_review" value="{{ $total_active_review }}">
                <input type="hidden" id="total_pending_review" value="{{ $total_pending_review }}">
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-12">
            <div class="card p-3 rounded-0 table-responsive">
                <h2 class="text-center font-20 font-pt">All Orders</h2>
                <hr>
                <table class="table table-striped table-dark display " id="dataTable">
                    <thead>
                        <tr align="center">
                            <th scope="col">No</th>
                            <th scope="col">Id</th>
                            <th scope="col">Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Payment</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 0;
                            $total_orders = 0;
                            $total_completed_order = 0;
                            $total_confirm_order = 0;
                            $total_pending_order = 0;
                            $purchase_quantity = 0;
                            $purchase_cost = 0;
                        @endphp
                        @foreach ($customer->orders as $order)
                            @php
                                $total_orders++;
                                $i++;
                                if ($order->status == 'Confirm') {
                                    $total_confirm_order = 1;
                                    $purchase_quantity += $order->total_quantity;
                                } elseif ($order->status == 'Pending') {
                                    $total_pending_order++;
                                }
                                if ($order->process == 100) {
                                    $total_completed_order++;
                                }
                                if ($order->payment == 'Confirm') {
                                    $purchase_cost += $order->total_cost;
                                }
                            @endphp
                            <tr align="center">
                                <th class="font-pt font-18">{{ $i }}</th>
                                <td class="font-pt font-18">{{ $order->order_code }}</td>
                                <td class="font-pt font-18">{{ $order->created_at->format('d-m-Y') }}</td>
                                <td class="font-pt font-18">{{ $order->status }}</td>

                                <td class="font-pt font-18">{{ $order->payment }}</td>
                                <td class="font-pt font-18">
                                    <a data-toggle="tooltip" data-placement="top" title="View" class="btn btn-info"
                                        href="{{ route('admin.order.show', ['id' => $order->id]) }}">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a data-toggle="tooltip" data-placement="top" title="Invoice" class="btn btn-primary"
                                        href="{{ route('admin.order.invoice', ['id' => $order->id]) }}">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                    </a>

                                    <a data-toggle="tooltip" data-placement="top" title="Delete"
                                        class="btn btn-danger delete-order" href="#" data-id="{{ $order->id }}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer-section')
    <script>
        $(document).ready(function() {

            $("#show_total_order").html({{ $total_orders }});
            $("#show_total_confirm_order").html($("#total_confirm_order").val());
            $("#show_total_pending_order").html($("#total_pending_order").val());

            $("#show_purchase_quantity").html($("#purchase_quantity").val());
            $("#show_purchase_cost").html($("#purchase_cost").val());

            $("#show_total_review").html({{ $total_review }});
            $("#show_total_active_review").html($("#total_active_review").val());
            $("#show_total_pending_review").html($("#total_pending_review").val());

        })
    </script>
@endsection
