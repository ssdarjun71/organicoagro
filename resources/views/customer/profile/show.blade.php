@extends('customer.layouts.master')

@section('title')
{{Auth::user()->name}}
@endsection



@section('content')

	<div class="card">
		<div class="card-header">
			<h4 class="font-pt font-25 text-center">Information</h4>
		</div>
		
		<div class="card-body row">
			<div class="col-12 col-lg-3 text-center pt-3">
				@if(Auth::user()->image == '')
				<img width="100px;" src="{{set_path('assets/img/default/user.png')}}" alt="{{Auth::user()->name}}">
				@else

				@endif
			</div>
			<div class="col-12 col-lg-9">
				<ul>
					<li><b>Designation:</b> {{Auth::user()->designation}}</li>
					<li><b>Name:</b> {{Auth::user()->name}}</li>
					<li><b>Email:</b> {{Auth::user()->email}}</li>
					<li><b>Phone:</b> {{Auth::user()->phone}}</li>
					<li><b>Address:</b> {{Auth::user()->address}}</li>
				</ul>
			</div>
		</div>
	</div>

@endsection


















