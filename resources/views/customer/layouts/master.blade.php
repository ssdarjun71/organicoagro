<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        {{-- 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        --}}
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="author" content="John Doe">
        @yield('seo')
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Place favicon.ico in the root directory -->
        <link rel="shortcut icon" type="image/x-icon" href="{{set_path('assets/img/favicon.ico')}}">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Bootstrap Fremwork Main Css -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/bootstrap.min.css')}}">
        <!-- All Plugins css -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/plugins.css')}}">
        <!-- Theme shortcodes/elements style -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/shortcode/shortcodes.css')}}">
        <!-- Theme main style -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/style.css')}}">
        <!-- Responsive css -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/responsive.css')}}">
        <!-- User style -->
        <link rel="stylesheet" href="{{set_path('assets/front-end/css/custom.css')}}">
        <!-- Modernizr JS -->
        <script src="{{set_path('assets/front-end/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    
        @yield('custom-css')
    </head>
    <body id="main-body">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->  
        <div id="notificaton-div">
            <span id="notificaton-text"></span>
        </div>
        <div id="cart-side" class="cart-side cart__menu">
            <span class="ti-shopping-cart"><span class="total_cart_products" ></span></span>
        </div>


        <!-- Start Header Style -->
        @include('includes.header')
        <!-- End Header Style -->

        <!-- Body main wrapper start -->
        <div class="wrapper home-9 wrap__box__style--1">

            <div class="body__overlay"></div>

            <!-- main body -->
            <div class="page-content">
                <div class="bg--4 ">
                    <!-- page content -->

                    <div class="container py-3">
                        <div class="customer-dashboard">
                            <div class="customer-dashboard-header">
                                @include('customer.layouts.menu')
                            </div>
                            
                            <div class="customer-dashboard-content p-3">
                                @if ($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-danger">
                                            {{ $error }}
                                        </div>
                                    @endforeach
                                @endif

                                @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                @endif


                                @yield('content')
                            </div>
                        </div>
                    </div>
                    <!-- end page content -->


                    <!-- Start Footer Area -->
                        @include('includes.footer')
                    <!-- End Footer Area -->

                </div>
            </div>
            <!-- end main body -->
        </div>
        <!-- Body main wrapper end -->
        <!-- Placed js at the end of the document so the pages load faster -->
        <!-- jquery latest version -->
        <script src="{{set_path('assets/front-end/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <!-- Bootstrap Framework js -->
        <script src="{{set_path('assets/front-end/js/popper.min.js')}}"></script>
        <script src="{{set_path('assets/front-end/js/bootstrap.min.js')}}"></script>
        <!-- All js plugins included in this file. -->
        <script src="{{set_path('assets/front-end/js/plugins.js')}}"></script>
        <!-- Main js file that contents all jQuery plugins activation. -->
        <script src="{{set_path('assets/front-end/js/main.js')}}"></script>
        <script>
            // ajax call setup header 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // end header setup 
        </script>
        {{-- add to cart --}}
        <script src="{{set_path('assets/front-end/js/add-to-cart.js')}}"></script>
        @yield('custom-js')
    </body>
</html>