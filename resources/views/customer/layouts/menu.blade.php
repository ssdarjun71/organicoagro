<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link @if(Request::is('customer')) active @endif" href="{{route('customer.home')}}">Dashboard</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(Request::is('customer/order') || Request::is('customer/order/*')) active @endif" href="{{route('customer.order')}}">Orders</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(Request::is('customer/reviews') || Request::is('customer/review/*')) active @endif" href="{{route('customer.reviews')}}">Review</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(Request::is('customer/profile')) active @endif" href="{{route('customer.profile')}}">Profile</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if( Request::is('customer/profile/edit')) active @endif" href="{{route('customer.profile.edit')}}">Edit Profile</a>
    </li>

    <li class="nav-item">
        <a class="nav-link @if(Request::is('customer/profile/edit/passowrd') || Request::is('customer/profile/edit/passowrd/*')) active @endif" href="{{route('customer.profile.edit.password')}}">Change Password</a>
    </li>
</ul>