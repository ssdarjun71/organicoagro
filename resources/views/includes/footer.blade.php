<footer class="htc__foooter__area bg__white footer--4">
    <div class="container">
        <div class="row py-5 clearfix">
            <!-- Start Single Footer Widget -->
            <div class="col-md-6 col-lg-3 col-sm-6">
                <div class="ft__widget">
                    <div class="ft__logo">
                        <a href="/">
                        <img src="{{set_path('assets/img/logo/uniqlo.png')}}" alt="footer logo">
                        </a>
                    </div>
                    <div class="footer__details">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus aliquam nihil est odio architecto perspiciatis velit modi consectetur! Voluptates, quia..</p>
                    </div>
                </div>
            </div>
            <!-- End Single Footer Widget -->
            <!-- Start Single Footer Widget -->
            <div class="col-md-6 col-lg-3 col-sm-6 smb-30 xmt-30">
                <div class="ft__widget">
                    <h2 class="ft__title">Quick Menu</h2>
                    <div class="newsletter__form">
                        <div class="input__box">
                            <div id="mc_embed_signup" class="footer-widget">
                                <ul>
                                    <li>
                                        <a href="{{route('website.about_page')}}">About Us</a>
                                    </li>
                                    <li>
                                        <a href="{{route('website.contact_page')}}">Contact Us</a>
                                    </li>
                                    <li>
                                        <a href="{{route('website.faq_page')}}">FAQ</a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{route('website.returnpolicy')}}">Return Policy</a>
                                    </li>
                                    <li>
                                        <a href="{{route('website.privacy_page')}}">Privacy Policy</a>
                                    </li>
                                    <li>
                                        <a href="{{route('website.condition_page')}}">Terms and Conditions</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Footer Widget -->
            <!-- Start Single Footer Widget -->
            <div class="col-md-6 col-lg-3 col-sm-6 smt-30 xmt-30">
                <div class="ft__widget contact__us">
                    <h2 class="ft__title">Contact Us</h2>
                    <div class="footer__inner">
                        <p>Dhanmondi 15<br>Dhaka, Bangladesh</p>
                    </div>
                </div>
            </div>
            <!-- End Single Footer Widget -->
            <!-- Start Single Footer Widget -->
            <div class="col-md-6 col-lg-2 lg-offset-1 col-sm-6 smt-30 xmt-30">
                <div class="ft__widget follow-us">
                    <h2 class="ft__title">Follow Us</h2>
                    <ul class="social__icon">
                        <li><a href="https://twitter.com/" target="_blank"><i class="zmdi zmdi-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com//" target="_blank"><i class="zmdi zmdi-instagram"></i></a></li>
                            <li><a href="https://www.facebook.com/" target="_blank"><i class="zmdi zmdi-facebook"></i></a></li>
                            <li><a href="https://plus.google.com/" target="_blank"><i class="zmdi zmdi-google-plus"></i></a></li>
                        </ul>
                    </ul>
                </div>
            </div>
            <!-- End Single Footer Widget -->
        </div>
        
    </div>
    <!-- Start Copyright Area -->
    <div class="htc__copyright__area">
        <div class="container">
            <div class="copyright__inner">
                <div class="copyright">
                    <p>Copyright © 2020 | All rights reserved by - <a target="_blank" href="https://intradocint.com" target="_blank">IntradocInt</a></p>
                </div>
                <ul class="footer__menu">
                    <li><img src="https://mironmahmud.com/vegana/assets/ltr-version/images/pay-card/01.jpg" alt=""></li>
                    <li><img src="https://mironmahmud.com/vegana/assets/ltr-version/images/pay-card/02.jpg" alt=""></li>
                    <li><img src="https://mironmahmud.com/vegana/assets/ltr-version/images/pay-card/03.jpg" alt=""></li>
                    <li><img src="https://mironmahmud.com/vegana/assets/ltr-version/images/pay-card/04.jpg" alt=""></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Copyright Area -->
    <audio style="position: absolute; top: -100%; opacity: 0;"  id="error_sound" src="{{set_path('assets/audio/error.mp3')}}" preload="auto"></audio>
    <audio style="position: absolute; top: -100%; opacity: 0;"  id="success_sound" src="{{set_path('assets/audio/success.mp3')}}" preload="auto"></audio>
</footer>