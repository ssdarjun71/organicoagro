<header id="header" class="htc-header header--3">
    <div class="header-part">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="header-info">
                        <li><i class="fas fa-envelope"></i><p>info@organicoagro.com</p></li>
                        <li><i class="fas fa-phone-alt"></i><p>+8801712520018</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Mainmenu Area -->
    <div id="sticky-header-with-topbar" class="mainmenu__area sticky__header">
        <div class="container" >
            <div class="row">
                <div id="scroll-logo" class="col-md-2 col-lg-2 col-6">
                    <div class="logo">
                        <a href="{{route('website.home')}}">
                        <img src="{{set_path('assets/img/logo/uniqlo.png')}}" alt="logo">
                        </a>
                    </div>
                </div>


                <div id="scroll-icon" class="col-md-10 col-lg-2 col-6  d-block d-lg-none ">
                    <ul class="menu-extra text-right right-widget">
                        @auth
                            <li><a href="{{route('customer.home')}}"><span class="ti-user"></span></a></li>
                            <li>
                                <a  href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <span class="ti-export"></span></a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @else
                            <li><a href="{{route('website.customer.login')}}"><span class="ti-user"></span></a></li>
                        @endauth
                        
                        <li class="wisthlist__menu"><span class="ti-heart"><span class="total_wishlist_products" ><sup>0</sup></span></span></li>
                        <li class="cart__menu"><span class="ti-shopping-cart"><span class="total_cart_products" ><sup>0</sup></span></span></li>
                    </ul>
                </div>


                <!-- Start MAinmenu Ares -->
                <div id="scroll-search" class="col-md-12 col-lg-7 m-lg-0 mb-3">
                    <form action="{{route('website.search')}}" method="POST" class="search-form">
                        @csrf
                        <label class="sr-only" for="inlineFormInputGroupUsername">Search Here...</label>
                        <div class="input-group">
                            <input required name="keyword" type="text" class="form-control" id="inlineFormInputGroupUsername" placeholder="Search Here...">
                            <div class="input-group-prepend">
                                <button type="submit" class="ti-search border-0 btn btn-inline btn-success"> search</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
                <!-- End MAinmenu Ares -->
                <div class="col-md-2 col-lg-3 col-6 d-none d-lg-block ">
                    <ul class="menu-extra m-0 text-right right-widget">
                        @auth
                            <li><a href="{{route('customer.home')}}"><span class="ti-user"></span></a></li>
                            <li>
                                <a  href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <span class="ti-export"></span></a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @else
                            <li><a href="{{route('website.customer.login')}}"><span class="ti-user"></span></a></li>
                        @endauth

                        <li class="wisthlist__menu"><a href="{{ route('website.wishlist.index') }}"><span class="ti-heart"><span class="total_wishlist_products" ><sup>0</sup></span></span></a></li>

                        <li class="cart__menu"><span class="ti-shopping-cart"><span class="total_cart_products" ><sup>0</sup></span></span></li>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
    <!-- End Mainmenu Area -->
    <div class="header-style-two header-bottom">
        <div class="container px-0 px-md-2">
        <div class="menu-wrap">
            <nav class="menu-nav show">
                <div class="header-category">
                    <a href="#" class="cat-toggle"><i class="fas fa-bars"></i>Categories<i class="fas fa-angle-down"></i></a>
                    
                    <?php 
                        GLOBAL $nav;
                        GLOBAL $allready_print;
                        GLOBAL $total_prints;
                        GLOBAL $total_category;
                        
                        
                        $nav = '<ul class="category-menu">';
                        $allready_print = array(1);
                        $total_prints = 1;
                        $total_category = $categories->count();
                        
                        function parent_child($categories){
                        
                            GLOBAL $nav;
                            GLOBAL $allready_print;
                            GLOBAL $total_prints;
                            GLOBAL $total_category;
                            foreach ($categories as  $category) {
                        
                                if($category->left_nav == 0){
                                    continue;
                                }
                        
                                if(!in_array($category->id,$allready_print)){ // not print yeat
                                    if($category->childs->count() > 0){ // yes parent
                                        /*                                                                    
                                        $nav .= '<li><a href="#">'.$category->name.'<span>></span></a><ul>';
                                        parent_child($category->childs);
                                        $nav .='</ul></li>';
                                        */
                                    }else{
                                        //$nav .= '<li><a href="/category/'.$category->slug.'">'.$category->name.'</a></li>';
                                        $nav .= '<li><a href="/category/'.$category->slug.'">'.$category->name.'</a></li>';
                                    
                                    }
                                    $total_prints++;
                                    array_push($allready_print,$category->id);
                                }
                            }
                        
                            if($total_prints == $total_category){
                                return $nav;
                            }
                        }
                        parent_child($categories);
                        $nav .= '</ul>';
                        echo $nav;
                    ?>
                </div>
                <div class="navbar-wrap main-menu d-none d-lg-flex">
                    <ul class="navigation">
                        {{-- <li class="active dropdown"><a href="#">Home</a>
                            <ul class="submenu">
                                <li><a href="#">Home One</a></li>
                                <li class="active"><a href="#">Home Two</a></li>
                                <li><a href="#">Home Three</a></li>
                            </ul>
                            <div class="dropdown-btn"><span class="fas fa-angle-down"></span></div>
                        </li> --}}

                        <li><a href="{{route('website.home')}}">Home</a></li>
                        <li><a href="{{route('website.about_page')}}">About</a></li>
                        <li><a href="{{route('website.shop_page')}}">Shop</a></li>
                        <li><a href="{{route('website.contact_page')}}">contacts</a></li>
                        <li><a href="{{route('website.faq_page')}}">Faq</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        </div>
    </div>
</header>