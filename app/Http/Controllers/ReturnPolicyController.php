<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Returnpolicy;
use Illuminate\Support\Facades\Cache;
class ReturnPolicyController extends Controller
{
  


	public function index(){
		$returnpolicy = Returnpolicy::find(1);
		return view('website.pages.returnpolicy',compact('returnpolicy'));
	}

	public function edit(){
		$returnpolicy = Returnpolicy::find(1);
		return view('admin.pages.returnpolicy',compact('returnpolicy'));
	}


	public function update(Request $r){
		$returnpolicy = Returnpolicy::find(1);

		$r->validate([
		    'tag' => 'required',
		    'description' => 'required',
		    'text' => 'required'
		]);

		$returnpolicy->tag = $r->tag;
		$returnpolicy->description = $r->description;
		$returnpolicy->returnpolicy = $r->text;
		$returnpolicy->save();
		Cache::forget('returnpolicy-page');
		return back()->with('success','Terms And returnpolicy Update Success');
	}
}
