<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class WishlistController extends Controller
{
    
    
    public function index(){

        if(isset($_COOKIE['wishlist_products'])){
            $wishlist_products = explode(',', $_COOKIE['wishlist_products']);
        }else{
            $wishlist_products = [];
        }

        $products = Product::whereIn('id', $wishlist_products)->orderBy('id','DESC')->get();

        return view('public.wishlist.index', compact('products'));
    }
}
