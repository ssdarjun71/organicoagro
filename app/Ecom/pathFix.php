<?php

if(!function_exists('set_path')){
    function set_path($path){

        if(config("app.env") != "local"){
            $path_prifix = "/public/";
            return URL::to('/').$path_prifix.$path;
        }else{
            $path_prifix = "/";
            return URL::to('/').$path_prifix.$path;
        }
        
        
    }
}