<?php

use Illuminate\Database\Seeder;
use App\Returnpolicy;
class ReturnpolicySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $returnpolicy = new Returnpolicy;

        $returnpolicy->tag = "tag";
        $returnpolicy->description = "description";
        $returnpolicy->returnpolicy = "returnpolicy";

        $returnpolicy->save();
    }
}
