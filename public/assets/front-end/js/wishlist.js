

$(document).ready(function(){

    var wishlist_products = [];

    if(Cookies.get('wishlist_products')){
        old_wishlist_products = Cookies.get('wishlist_products');
        old_wishlist_products = old_wishlist_products.split(',');

        var wishlist_products = old_wishlist_products;
    }
    


    $(".add-wishlist").click(function(e){
        product_id = $(this).attr('product-id');
        
        var product_index = wishlist_products.indexOf(product_id);

        if (product_index == -1) {
            wishlist_products.push(product_id);// add product in wishlist
            $(this).addClass( "active" );
        }else if(product_index !== -1){
            wishlist_products.splice(product_index, 1);
            $(this).removeClass( "active" );
        }


        total_wish_list_products = wishlist_products.length;
        
        $('.wisthlist__menu .total_wishlist_products sup').html(total_wish_list_products);

        Cookies.set('wishlist_products', wishlist_products);
        Cookies.set('total_wish_list_products', total_wish_list_products);
    });


    $('.wisthlist__menu .total_wishlist_products sup').html(Cookies.get('total_wish_list_products'));

});